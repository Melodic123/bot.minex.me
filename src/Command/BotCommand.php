<?php
/**
 * Created by PhpStorm.
 * User: melodic
 * Date: 16.08.17
 * Time: 14:41
 */

namespace Minex\Bot\Exchange;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BotCommand extends Command
{


    /**
     * BotCommand constructor.
     */
    public function __construct()
    {
        parent::__construct('bot:start');
    }

    public function configure()
    {
        $this->addArgument('host', InputArgument::REQUIRED)->
        addArgument('login', InputArgument::OPTIONAL, 'Bot login', 'test2@yoldi.ru')->
        addArgument('password', InputArgument::OPTIONAL, 'Bot password', 'test');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Minex.me Exchange test bot');


        $host = $input->getArgument('host');

        $jar = new CookieJar();

        $client = new Client([
            'base_uri' => $host,
            RequestOptions::COOKIES => $jar,
            RequestOptions::ALLOW_REDIRECTS => false,
        ]);

        $output->writeln('Login as bot...');

        $response = $client->post('/auth/login', [
            RequestOptions::FORM_PARAMS => [
                'email' => $input->getArgument('login'),
                'password' => $input->getArgument('password')
            ]
        ]);

        if ($response->getStatusCode() !== 302) {
            $output->writeln('Login failed. Exit.');
            return;
        }

        $output->writeln('Login successful. Fetch order book list...');

        $response = $client->get('/exchange/order_books.json');

        if ($response->getStatusCode() !== 200) {
            $output->writeln('Fetching order book list failed. Exit.');
            return;
        }

        $orderBooks = json_decode($response->getBody()->getContents(), true);

        while (true) {
            rand(0, 1) ? $this->createAskOrder($client, $orderBooks) : $this->createBidOrder($client, $orderBooks);
            usleep(rand(800, 1000));
        }

    }

    /**
     * @param array $orderBooks
     * @return array
     */
    private function chooseOrderBook(array $orderBooks): array
    {
        return $orderBooks[rand(0, count($orderBooks) - 1)];
    }

    private function createAskOrder(Client $client, array $orderBooks)
    {
        $book = $this->chooseOrderBook($orderBooks);
        $amount = $this->chooseAmount($book['baseCurrency']);

        $response = $client->post('/exchange/create_ask_limit_order', [
            RequestOptions::FORM_PARAMS => [
                'order_book_id' => $book['id'],
                'amount' => $amount,
                'price' => $this->chooseCost($book['baseCurrency'], $book['quoteCurrency'])
            ]
        ]);
        if ($response->getStatusCode() !== 201) {
            throw new \Exception('Error while creating order');
        }
    }

    private function createBidOrder(Client $client, array $orderBooks)
    {
        $book = $this->chooseOrderBook($orderBooks);
        $amount = $this->chooseAmount($book['baseCurrency']);
        $response = $client->post('/exchange/create_bid_limit_order', [
            RequestOptions::FORM_PARAMS => [
                'order_book_id' => $book['id'],
                'amount' => $amount,
                'price' => $this->chooseCost($book['baseCurrency'], $book['quoteCurrency'])
            ]
        ]);
        if ($response->getStatusCode() !== 201) {
            throw new \Exception('Error while creating order');
        }

    }

    private function chooseAmount($currency)
    {
        switch ($currency) {
            case 'BTC':
                return rand(1, 20000) / 10000;
            case 'DASH':
            case 'ETH':
                return rand(1, 20000) / 1000;
            case 'LTC':
                return rand(10, 3000) / 100;
            case 'USD':
                return rand(1000, 10000) / 10;

        }
    }

    private function chooseCost($baseCurrency, $quoteCurrency)
    {
        $multiplier = rand(0, 1) ? 1 : -1;

        $percent = rand(10, 30) / 10;

        if ($baseCurrency === 'BTC' && $quoteCurrency === 'USD') {
            $base = 4000;
        }

        if ($baseCurrency === 'BTC' && $quoteCurrency === 'RUB') {
            $base = 240000;
        }
        if ($baseCurrency === 'DASH' && $quoteCurrency === 'BTC') {
            $base = 0.054;
        }
        if ($baseCurrency === 'DASH' && $quoteCurrency === 'USD') {
            $base = 216;
        }
        if ($baseCurrency === 'DASH' && $quoteCurrency === 'RUB') {
            $base = 13000;
        }

        if ($baseCurrency === 'ETH' && $quoteCurrency === 'BTC') {
            $base = 0.072;
        }
        if ($baseCurrency === 'ETH' && $quoteCurrency === 'LTC') {
            $base = 6.8;
        }
        if ($baseCurrency === 'ETH' && $quoteCurrency === 'USD') {
            $base = 290;
        }
        if ($baseCurrency === 'ETH' && $quoteCurrency === 'RUB') {
            $base = 17512;
        }
        if ($baseCurrency === 'LTC' && $quoteCurrency === 'BTC') {
            $base = 0.01;
        }
        if ($baseCurrency === 'LTC' && $quoteCurrency === 'USD') {
            $base = 42;
        }
        if ($baseCurrency === 'LTC' && $quoteCurrency === 'RUB') {
            $base = 2560;
        }
        if ($baseCurrency === 'USD' && $quoteCurrency === 'RUB') {
            $base = 60;
        }


        if (!isset($base))
            throw new \Exception('Wrong currencies');

        return $base - ($base / 100 * $percent * $multiplier);

    }
}