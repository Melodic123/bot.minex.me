#!/usr/bin/env php
<?php

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new \Minex\Bot\Exchange\BotCommand());

$application->run();
